package main

import "fmt"

type rangeBounds struct {
    b1, b2 float64
}
 
func mapRange(x, y rangeBounds, n float64) float64 {
    return y.b1 + (n - x.b1) * (y.b2 - y.b1) / (x.b2 - x.b1)
}
 
func main() {
    r1 := rangeBounds{1, 25}
    r2 := rangeBounds{220, 7221}
    for n := float64(1); n<26; n=n+1 {
//        fmt.Println(n, "maps to", mapRange(r1, r2, n))
        b := mapRange(r1, r2, n)
        fmt.Println( int( b ))
    }
}
