package main

import (
	"bufio"
	"crypto/sha256"
	"database/sql"
	"emptyspace/es"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/misterunix/godice"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pmcxs/hexgrid"
	"github.com/schollz/progressbar"
)

var universesystems int
var planets int
var players int
var planetcount int

// Planets : structs containing the planets
var Planets []es.Planet

// Systems : struct containing the systems
var Systems []es.System

// Players : struct containing fake customers
var Players []es.DBPlayer

/*
type LocationStruct struct {
	Q        int
	R        int
	Ore      int
	Organics int
}
*/

// CreateNewDB : Creates a new database if it doesnt exsist or deletes and creates the tables if it does.
func CreateNewDB() {

	/*
		database, _ := sql.Open("sqlite3", "../sqldata/emptyspace.db")
		statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS universe (id INTEGER PRIMARY KEY, q INTEGER, r INTEGER, s INTEGER, name TEXT)")
		statement.Exec()
	*/
	database, _ := sql.Open("sqlite3", "../sqldata/emptyspace.db")

	var sqlstatement string

	statement, _ := database.Prepare("DROP TABLE IF EXISTS system")
	statement.Exec()

	statement, _ = database.Prepare("DROP TABLE IF EXISTS planet")
	statement.Exec()

	statement, _ = database.Prepare("DROP TABLE IF EXISTS player")
	statement.Exec()

	tsystem := es.NewSystem()
	e := reflect.ValueOf(&tsystem).Elem()
	for i := 0; i < e.NumField(); i++ {
		var vt string
		varName := e.Type().Field(i).Name
		sqlstatement += "," + varName + " "
		varType := e.Type().Field(i).Type
		switch varType.Name() {
		case "int":
			if varName == "ID" {
				vt = "INTEGER NOT NULL PRIMARY KEY"
			} else {
				vt = "INTEGER"
			}
		case "string":
			vt = "TEXT"
		}
		sqlstatement += vt
	}
	sqlstatement = sqlstatement[1:]
	//fmt.Println(sqlstatement)

	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS system (" + sqlstatement + ")")
	statement.Exec()

	sqlstatement = ""
	tplanet := es.NewPlanet()
	e = reflect.ValueOf(&tplanet).Elem()
	for i := 0; i < e.NumField(); i++ {
		var vt string
		varName := e.Type().Field(i).Name
		sqlstatement += "," + varName + " "
		varType := e.Type().Field(i).Type
		switch varType.Name() {
		case "int":
			if varName == "ID" {
				vt = "INTEGER NOT NULL PRIMARY KEY"
			} else {
				vt = "INTEGER"
			}
		case "string":
			vt = "TEXT"
		}
		sqlstatement += vt
	}
	sqlstatement = sqlstatement[1:]
	//fmt.Println(sqlstatement)

	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS planet (" + sqlstatement + ")")
	statement.Exec()

	sqlstatement = ""
	tplayer := es.NewDBPlayer()
	e = reflect.ValueOf(&tplayer).Elem()
	for i := 0; i < e.NumField(); i++ {
		var vt string
		varName := e.Type().Field(i).Name
		sqlstatement += "," + varName + " "
		varType := e.Type().Field(i).Type
		switch varType.Name() {
		case "int":
			if varName == "ID" {
				vt = "INTEGER NOT NULL PRIMARY KEY"
			} else {
				vt = "INTEGER"
			}
		case "string":
			vt = "TEXT"
		}
		sqlstatement += vt
	}
	sqlstatement = sqlstatement[1:]
	//fmt.Println(sqlstatement)

	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS player (" + sqlstatement + ")")
	statement.Exec()

}

// CreateUniverse : populate items in the database
func CreateUniverse() {

	fmt.Println("Creating systems in memory")
	bar1 := progressbar.NewOptions(
		universesystems,
		progressbar.OptionSetTheme(progressbar.Theme{Saucer: "#", SaucerPadding: "-", BarStart: ">", BarEnd: "<"}),
		progressbar.OptionSetWidth(50),
	)
	for i := 0; i < universesystems; i++ {
		s := es.NewSystem()
		s.ID = i
		for {
			s.Q = godice.Roll(1, 300)
			s.R = godice.Roll(1, 300)
			A := hexgrid.NewHex(s.Q, s.R)
			bad := 0
			if i > 0 {
				for j := 0; j < i; j++ {
					if i == j {
						continue
					}
					B := hexgrid.NewHex(Systems[j].Q, Systems[j].R)
					C := hexgrid.HexDistance(A, B)
					if C > 10 {
						bad = 0
						break // Might have broken code here
					} else {
						bad = 1
					}
				}
			}
			if bad == 0 {
				break
			}
		}
		name := "Unknown System " + strconv.Itoa(i)
		s.Name = name
		Systems = append(Systems, s)
		bar1.Add(1)
	}
	bar1.Reset()
	fmt.Println()

	/*
		planetcount := 0
		fmt.Println("Creating planets in memory.")
		fmt.Println("universesystems:", universesystems)
		for i := 0; i < universesystems; i++ {
			np := godice.Roll(1, 8)
			for j := 0; j < np; j++ {
				pl := es.NewPlanet()
				pl.Id = planetcount
				pl.SystemId = i
				name := "System " + strconv.Itoa(i) + " Planet " + strconv.Itoa(j)
				pl.Name = name
				pl.PType = godice.Roll(1, 10)
				Planets = append(Planets, pl)
				planetcount++
			}
			bar1.Add(1)
		}
		bar1.Reset()
		fmt.Println()
	*/
	MakePlanets()
	fmt.Println("Planet Count:", planetcount)

	MakePlayers(planetcount)
	/*
		fmt.Println("Creating a few NPCs. Player 0 is always admin.")
		for i := 1; i < godice.Roll(1, 20); i++ {
			np := es.NewDBPlayer()
			np.Name = "Player " + strconv.Itoa(i)
			np.HomeWorldID = godice.Roll(1, planetcount)
			np.Username = "player" + strconv.Itoa(i)
			sha := sha256.New()
			sha.Write([]byte("BadPassword"))
			cp := fmt.Sprintf("%x\n", sha.Sum(nil))
			np.Password = cp
			Players = append(Players, np)
			fmt.Println(np.Name, np.Password)
			Planets[np.HomeWorldID].PlayerID = i
		}
	*/
}

// SaveUniverseDB : Save the universe to the DB.
func SaveUniverseDB() {
	database, _ := sql.Open("sqlite3", "../sqldata/emptyspace.db")
	var sqlstatement string

	fmt.Println("Saving systems to DB")
	bar2 := progressbar.NewOptions(
		universesystems,
		progressbar.OptionSetTheme(progressbar.Theme{Saucer: "#", SaucerPadding: "-", BarStart: ">", BarEnd: "<"}),
		progressbar.OptionSetWidth(50),
		//progressbar.OptionSetWriter(&buf),
	)
	for _, v := range Systems {
		var ssql, vsql string
		e := reflect.ValueOf(&v).Elem()
		for i := 0; i < e.NumField(); i++ {
			varName := e.Type().Field(i).Name
			ssql += "," + varName
			varValue := e.Field(i).Interface()
			vv := fmt.Sprintf("%v", varValue)
			varType := e.Type().Field(i).Type
			switch varType.Name() {
			case "int":
				vsql += "," + vv
			case "string":
				vsql += "," + "'" + vv + "'"
			}
		}
		ssql = ssql[1:]
		vsql = vsql[1:]
		sqlstatement = "INSERT INTO system( " + ssql + " ) VALUES(" + vsql + ")"
		//fmt.Println(sqlstatement)
		statement, _ := database.Prepare(sqlstatement)
		statement.Exec()
		bar2.Add(1)
	}
	bar2.Reset()
	fmt.Println()

	fmt.Println("Saving planets to DB")
	pc := len(Planets)
	bar3 := progressbar.NewOptions(
		pc,
		progressbar.OptionSetTheme(progressbar.Theme{Saucer: "#", SaucerPadding: "-", BarStart: ">", BarEnd: "<"}),
		progressbar.OptionSetWidth(50),
	)

	for _, v := range Planets {
		var ssql, vsql string
		e := reflect.ValueOf(&v).Elem()
		for i := 0; i < e.NumField(); i++ {
			varName := e.Type().Field(i).Name
			if varName == "ID" {
				continue
			}
			ssql += "," + varName
			varValue := e.Field(i).Interface()
			vv := fmt.Sprintf("%v", varValue)
			varType := e.Type().Field(i).Type
			switch varType.Name() {
			case "int":
				vsql += "," + vv
			case "string":
				vsql += "," + "'" + vv + "'"
			}
		}
		ssql = ssql[1:]
		vsql = vsql[1:]
		sqlstatement = "INSERT INTO planet( " + ssql + " ) VALUES(" + vsql + ")"
		//fmt.Println(sqlstatement)
		statement, _ := database.Prepare(sqlstatement)
		statement.Exec()
		bar3.Add(1)
	}
	bar3.Reset()
	fmt.Println()

	fmt.Println("Saving players to DB")
	plc := len(Players)
	bar4 := progressbar.NewOptions(
		plc,
		progressbar.OptionSetTheme(progressbar.Theme{Saucer: "#", SaucerPadding: "-", BarStart: ">", BarEnd: "<"}),
		progressbar.OptionSetWidth(50),
	)

	for _, v := range Players {
		var ssql, vsql string
		e := reflect.ValueOf(&v).Elem()
		for i := 0; i < e.NumField(); i++ {
			varName := e.Type().Field(i).Name
			if varName == "ID" {
				continue
			}
			ssql += "," + varName
			varValue := e.Field(i).Interface()
			vv := fmt.Sprintf("%v", varValue)
			varType := e.Type().Field(i).Type
			switch varType.Name() {
			case "int":
				vsql += "," + vv
			case "string":
				vsql += "," + "'" + vv + "'"
			}
		}
		ssql = ssql[1:]
		vsql = vsql[1:]
		sqlstatement = "INSERT INTO player( " + ssql + " ) VALUES(" + vsql + ")"
		statement, err := database.Prepare(sqlstatement)
		if err != nil {
			panic(err)
		}
		statement.Exec()
		bar4.Add(1)
	}
	bar4.Reset()
	fmt.Println()

}

func MakePlanets() {
	//planetcount := 0
	fmt.Println("Creating planets in memory.")
	fmt.Println("universesystems:", universesystems)
	for i := 0; i < universesystems; i++ {
		np := godice.Roll(1, 8) // Number of planets in the system
		for j := 0; j < np; j++ {
			pl := es.NewPlanet()
			//pl.Id = planetcount
			pl.SystemID = i
			name := "System " + strconv.Itoa(i) + " Planet " + strconv.Itoa(j)
			pl.Name = name
			pl.PType = godice.Roll(4, 4) - 4
			Planets = append(Planets, pl)
			planetcount++
		}
		//		bar1.Add(1)
	}
	//bar1.Reset()
	fmt.Println()
	//	fmt.Println(planetcount)

}

func MakePlayers(planetcount int) {
	// admin password show done from the CLI
	fmt.Println("Creating a few NPCs. Player 1 is always admin.")
	//for i := 1; i < godice.Roll(1, 20); i++ {
	for i := 0; i < 4; i++ {
		np := es.NewDBPlayer()
		np.Name = "Player " + strconv.Itoa(i)
		np.HomeWorldID = godice.Roll(1, planetcount)
		np.Username = "player" + strconv.Itoa(i)
		sha := sha256.New()
		sha.Write([]byte("BadPassword"))
		cp := fmt.Sprintf("%x", sha.Sum(nil))
		np.Password = cp
		Players = append(Players, np)
		fmt.Println(np.Name, np.Password)
		Planets[np.HomeWorldID].PlayerID = i
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Warning, this will clear the database a create a newone.")
	fmt.Println("Are you sure this is what you want to do?")
	//fmt.Println("y/n")
	for {
		fmt.Print("y/n > ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)
		if strings.Compare("y", text) == 0 {
			break
		} else {
			os.Exit(1)
		}
	}

	universesystems = 100 // This needs to be on the CLI with a default of 2000
	fmt.Printf("Number of systems in universe. min=100 > ")
	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	//fmt.Printf("text '%s'\n", text)
	num, err := strconv.Atoi(text)
	fmt.Println(text, num)
	if err != nil {
		fmt.Println("Could not convert text to integer.")
		panic(err)
	}
	if num < 100 {
		universesystems = 100
	} else {
		universesystems = num
	}
	fmt.Println("Systems in the Universe:", universesystems)

	planets = 0

	CreateNewDB()
	CreateUniverse()
	SaveUniverseDB()

}
