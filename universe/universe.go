package universe

type GameStats struct {
	UniverseMaxQ            int
	UniverseMaxR            int
	UniverseSystems         int
	SystemRatio             int
	UniverseSystemsDistance int
}

func NewGameStats(mq, mr int, sr float64) GameStats {
	gs := GameStats{}
	i := float64(mq * mr)
	ss := i * (sr / 100.0)
	u := int(ss)
	gs.UniverseMaxQ = mq
	gs.UniverseMaxR = mr
	gs.SystemRatio = int(sr)
	gs.UniverseSystems = u
	gs.UniverseSystemsDistance = 3

	return gs
}

type UData struct {
	ID               int
	Qhex             int
	Rhex             int
	Named            string
	Controlledby     int
	Gov              int
	Flags            int
	Ore              int
	Organics         int
	Equipment        int
	Tech             int
	Research         int
	Productionore    int
	Productionequ    int
	Completegov      int
	Completeore      int
	Completeequ      int
	Completetech     int
	Completeresearch int
	Population       int
}

func NewStarSystem() UData {
	d := UData{}
	return d
}

type PData struct {
	ID        int
	username  string
	email     string
	hashed_pw string
	active    int
}

func NewPlayer() PData {
	p := PData{}
	return p
}
