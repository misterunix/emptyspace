package es

type Planet struct {
	Id            int
	SystemId      int
	PType         int
	PlayerID      int
	Name          string
	Population    int
	TechLevel     int
	Goverment     int
	RawOre        int
	ProcessedOre  int
	RawFood       int
	ProcessedFood int
	Manufacturing int
	JumpFuel      int
	SublightFuel  int
	SpacePort     int
}

func NewPlanet() Planet {
	p := Planet{}
	p.Name = "Unknown"
	p.SystemId = 0
	p.PlayerID = 0
	p.PType = 0
	p.Population = 0
	p.TechLevel = 0
	p.Goverment = 0
	p.RawOre = 0
	p.ProcessedOre = 0
	p.RawFood = 0
	p.ProcessedFood = 0
	p.Manufacturing = 0
	p.JumpFuel = 0
	p.SublightFuel = 0
	p.SpacePort = 0
	return p
}

type System struct {
	Id   int
	Name string
	Q    int
	R    int
}

func NewSystem() System {
	s := System{}
	s.Id = 0
	s.Name = "Unknown"
	s.Q = 0
	s.R = 0
	return s
}

type DBPlayer struct {
	Id          int
	Name        string
	Username    string
	Password    string
	HomeWorldID int
}

func NewDBPlayer() DBPlayer {
	p := DBPlayer{}
	return p
}
