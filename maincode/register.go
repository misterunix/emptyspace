package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

func RegisterNewUser() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Warning, this will clear the database a create a newone.")
	fmt.Println("Are you sure this is what you want to do?")
	fmt.Println("y/n")
	for {
		fmt.Print("> ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)
		if strings.Compare("y", text) == 0 {
			break
		} else {
			os.Exit(1)
		}
	}
}
