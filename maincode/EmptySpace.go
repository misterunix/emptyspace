package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"strings"
	"text/template"

	_ "github.com/mattn/go-sqlite3"
)

// tsb

const (
	CONNHOST = "0.0.0.0"
	CONNPORT = "4444"
	CONNTYPE = "tcp"
)

type Player struct {
	Username string
	Userid   int
	Conn     net.Conn
	Index    int
}

var Players map[net.Conn]Player
var ConnChannel chan net.Conn

func check(err error, message string) {
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", message)
}

func random(min int, max int) int {
	return rand.Intn(max-min) + min
}

func main() {
	// Listen for incoming connections.

	go OpenComms()
	

	
}

/*
func getPlayersPlanets() int {
	q := "SELECT COUNT(*) FROM planet WHERE PlayerID='" + strconv.Itoa(1) + "'"
	rows, _ := database.Query(q)
	var t int
	for rows.Next() {
		rows.Scan(&t)
	}
	return t
}
*/

func handleRequest(c net.Conn) {

	fmt.Printf("Serving %s\n", c.RemoteAddr().String())

	var tr bytes.Buffer
	var s string

	type Welcome struct {
		SystemCount int
		PlanetCount int
	}

	welcome := Welcome{}
	welcome.SystemCount = 0
	welcome.PlanetCount = 0

	database, _ := sql.Open("sqlite3", "sqldata/emptyspace.db")

	q := "SELECT COUNT(*) FROM planet"
	rows, _ := database.Query(q)
	for rows.Next() {
		rows.Scan(&welcome.PlanetCount)
	}
	q = "SELECT COUNT(*) FROM system"
	rows, _ = database.Query(q)
	for rows.Next() {
		rows.Scan(&welcome.SystemCount)
	}

	buf1 := make([]byte, 3)
	buf1[0] = 0xff
	buf1[1] = 0xfb
	buf1[2] = 0x01

	c.Write(buf1)

	g, err := template.ParseFiles("templates/welcome.template")
	if err != nil {
		log.Println(err)
	}
	err = g.Execute(&tr, welcome)
	if err != nil {
		log.Println(err)
	}

	s = tr.String()

	c.Write([]byte(s))

	var cm string
	smallb := make([]byte, 1)
	for {
		fmt.Println("Top of for.")
		redlen, err := c.Read(smallb)

		if smallb[0] > 126 {
			continue
		}

		// Eat this byte
		if string(smallb) == "\r" {
			continue
		}

		if string(smallb) == "\n" {
			cm = strings.ToUpper(cm)
			if cm == "HELP" {
				c.Write([]byte("No help file at this time.\r\n"))
			}
			/*
				if cm == "PLIST" {
					u := getPlayersPlanets()
					tmp := strconv.Atoi(u)
					c.Write([]byte(tmp))
				}
			*/
			if cm == "STOP" {
				break
			}
			cm = ""
		} else {
			cm = cm + string(smallb)
			fmt.Println("---------------\nerr", err)
			fmt.Println("readlen", redlen)
			fmt.Println("smallb", string(smallb))
			fmt.Printf("cm: '%s'\n", cm)
		}
	}

	c.Close()
}

func stripCtlAndExtFromUTF8(str string) string {
	return strings.Map(func(r rune) rune {
		if r >= 32 && r < 127 {
			return r
		}
		return -1
	}, str)
}
