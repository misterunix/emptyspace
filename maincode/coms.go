package main

import (
	"fmt"
	"net"
	"os"
)

func OpenComms() {
	l, err := net.Listen(CONNTYPE, CONNHOST+":"+CONNPORT)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONNHOST + ":" + CONNPORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go HandleConnection(conn)
	}
}

func HandleConnection(c net.Conn) {

	// turn off echo on remote client
	tbuf1 := make([]tbyte, 3)
	tbuf1[0] = 0xff
	tbuf1[1] = 0xfb
	tbuf1[2] = 0x01
	c.Write(tbuf1)

}
