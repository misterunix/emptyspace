DROP TABLE IF EXISTS starsystems;
CREATE TABLE starsystems (
  ID SERIAL primary key,
  qhex int not null default 0,
  rhex int not null default 0,
  named text DEFAULT NULL,
  controlledby int not null default 0,
  gov int not null default 0,
  flags int not null default 0,
  ore int not null default 0,
  organics int not null default 0,
  equipment int not null default 0,
  tech int not null default 0,
  research int not null default 0,
  productionore int not null default 0,
  productionorg int not null default 0,
  productionequ int not null default 0,
  completegov int not null default 0,
  completeore int not null default 0,
  completeorg int not null default 0,
  completeequ int not null default 0,
  completetech int not null default 0,
  completeresearch int not null default 0,
  population int not null default 0
);
INSERT INTO starsystems values(0,1,1,'Sol',0,100,100,0,10000,10000,10000,100,100,15,15,15,1,1,1,1,1,1000000000);

DROP TABLE IF EXISTS players;
CREATE TABLE players (
  ID SERIAL primary key,
  username text DEFAULT NULL,
  email text DEFAULT NULL,
  hashed_pw text DEFAULT NULL,
  active boolean NOT NULL DEFAULT 'n'
);
INSERT INTO players VALUES (0,'admin','misterunix@gmail.com','####','y');
 